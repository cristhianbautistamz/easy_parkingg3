
package com.easyparking.easyparking.model;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
/**
 *
 * @author Usuario
 */
@Entity
@Table(name="tarifa")
public class Tarifa implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="idtarifa")
    private Integer idTarifa;
    
    @ManyToOne
    @JoinColumn(name="idparqueadero")
    private Parqueadero parqueadero;
    
    @Column(name="clientefrecuente")
    private String clienteFrecuente;
    
    @Column(name="valorfraccion")
    private String valorfraccion;

    public Integer getIdTarifa() {
        return idTarifa;
    }

    public void setIdTarifa(Integer idTarifa) {
        this.idTarifa = idTarifa;
    }

    public Parqueadero getParqueadero() {
        return parqueadero;
    }

    public void setParqueadero(Parqueadero parqueadero) {
        this.parqueadero = parqueadero;
    }

    public String getClienteFrecuente() {
        return clienteFrecuente;
    }

    public void setClienteFrecuente(String clienteFrecuente) {
        this.clienteFrecuente = clienteFrecuente;
    }

    public String getValorfraccion() {
        return valorfraccion;
    }

    public void setValorfraccion(String valorfraccion) {
        this.valorfraccion = valorfraccion;
    }
    
    
}
