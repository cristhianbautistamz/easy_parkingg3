
package com.easyparking.easyparking.model;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
/**
 *
 * @author Usuario
 */
@Entity
@Table(name="vehiculos")
public class Vehiculos implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="idvehiculos")
    private Integer idVehiculos;
    
    @ManyToOne
    @JoinColumn(name="idtipovehiculo")
    private TipoVehiculo tipoVehiculo;
    
    @Column(name="modelo")
    private String modelo;
    
    @Column(name="color")
    private String color;
      
    @Column(name="placa")
    private String placa;

    public Integer getIdVehiculos() {
        return idVehiculos;
    }

    public void setIdVehiculos(Integer idVehiculos) {
        this.idVehiculos = idVehiculos;
    }

    public TipoVehiculo getTipoVehiculo() {
        return tipoVehiculo;
    }

    public void setTipoVehiculo(TipoVehiculo tipoVehiculo) {
        this.tipoVehiculo = tipoVehiculo;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }
    
    
    
}
