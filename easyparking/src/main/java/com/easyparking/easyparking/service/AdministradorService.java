
package com.easyparking.easyparking.service;

import com.easyparking.easyparking.model.Administrador;
import java.util.List;

/**
 *
 * @author ASUS
 */
public interface AdministradorService {
    
 public Administrador save(Administrador administrador);
 public void delete(Integer id);
 public Administrador findById(Integer id);
 public List<Administrador> findAll();
}
