
package com.easyparking.easyparking.service;

import com.easyparking.easyparking.model.Parqueadero;
import java.util.List;

/**
 *
 * @author ASUS
 */
public interface ParqueaderoService {
    
 public Parqueadero save(Parqueadero parqueadero);
 public void delete(Integer id);
 public Parqueadero findById(Integer id);
 public List<Parqueadero> findAll();
    
}
