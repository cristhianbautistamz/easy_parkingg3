
package com.easyparking.easyparking.service.implement;

import com.easyparking.easyparking.dao.VehiculosDao;
import com.easyparking.easyparking.model.Vehiculos;
import com.easyparking.easyparking.service.VehiculosService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author ASUS
 */
@Service
public class VehiculosServiceImpl implements VehiculosService{
    
    @Autowired
    private VehiculosDao VehiculosDao;

    @Override
    @Transactional(readOnly = false)
    public Vehiculos save(Vehiculos vehiculos) {
        return VehiculosDao.save(vehiculos);
    }

    @Override
    @Transactional(readOnly = false)
    public void delete(Integer id) {
        VehiculosDao.deleteById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public Vehiculos findById(Integer id) {
        return VehiculosDao.findById(id).orElse(null);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Vehiculos> findAll() {
        return (List<Vehiculos>) VehiculosDao.findAll();
    }
    
    
}
