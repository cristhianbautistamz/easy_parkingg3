
package com.easyparking.easyparking.service.implement;

import com.easyparking.easyparking.dao.FacturaDao;
import com.easyparking.easyparking.model.Factura;
import com.easyparking.easyparking.service.FacturaService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
/**
 *
 * @author ASUS
 */
@Service
public class FacturaServiceImpl implements FacturaService{
    
 @Autowired
    private FacturaDao FacturaDao;

    @Override
    @Transactional(readOnly = false)
    public Factura save(Factura factura) {
        return FacturaDao.save(factura);
    }

    @Override
    @Transactional(readOnly = false)
    public void delete(Integer id) {
        FacturaDao.deleteById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public Factura findById(Integer id) {
        return FacturaDao.findById(id).orElse(null);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Factura> findAll() {
        return (List<Factura>) FacturaDao.findAll();
    }   
    
}
