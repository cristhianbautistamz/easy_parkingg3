
package com.easyparking.easyparking.service;

import com.easyparking.easyparking.model.Factura;
import java.util.List;

/**
 *
 * @author ASUS
 */
public interface FacturaService {

    public Factura save(Factura factura);

    public void delete(Integer id);

    public Factura findById(Integer id);

    public List<Factura> findAll();
}
