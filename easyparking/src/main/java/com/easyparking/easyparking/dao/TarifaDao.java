
package com.easyparking.easyparking.dao;

import com.easyparking.easyparking.model.Tarifa;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author Usuario
 */
public interface TarifaDao extends CrudRepository<Tarifa,Integer>{
    
}
