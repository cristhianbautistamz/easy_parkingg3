
package com.easyparking.easyparking.dao;

import com.easyparking.easyparking.model.TipoVehiculo;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author Usuario
 */
public interface TipoVehiculoDao extends CrudRepository<TipoVehiculo,Integer> {
    
}
