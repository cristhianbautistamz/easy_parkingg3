
package com.easyparking.easyparking.dao;

import com.easyparking.easyparking.model.Administrador;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author Usuario
 */
public interface AdministradorDao extends CrudRepository<Administrador,Integer> {
    
}
