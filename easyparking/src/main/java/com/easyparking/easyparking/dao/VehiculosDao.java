
package com.easyparking.easyparking.dao;

import com.easyparking.easyparking.model.Vehiculos;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author Usuario
 */
public interface VehiculosDao extends CrudRepository<Vehiculos,Integer>{
    
}
