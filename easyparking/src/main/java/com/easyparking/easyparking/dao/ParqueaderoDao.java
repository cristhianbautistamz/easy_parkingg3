
package com.easyparking.easyparking.dao;

import com.easyparking.easyparking.model.Parqueadero;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author Usuario
 */
public interface ParqueaderoDao extends CrudRepository<Parqueadero,Integer>{
    
}
